import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Dashboard } from "./pages/Dashboard";
import { Details } from "./pages/Details";

export const App = () => {

  return (
    <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route exact path="/:name" component={Details} />
        </Switch>
    </BrowserRouter>
  );
};
