export type Forecast = {
    id: number;
    name: string;
    temp: number;
    humidity: number;
    forecast: string;
    forecast_description: string;
    icon_id: string;
    location_id: number;
    time: string;
}