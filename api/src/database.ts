var mysql = require("mysql");
const dotenv = require("dotenv");
dotenv.config();

//DB Configuration
var dbConfig = {
  host: process.env.DB_SERVER,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PW,
  database: process.env.DB_NAME,
};

var db;

//If DB disconnects, reestablish connection
export const handleDisconnect = () => {
  db = mysql.createConnection(dbConfig);

  db.connect(function (err) {
    if (err) {
      console.log("Error while connection to DB", err);
      setTimeout(handleDisconnect, 2000);
    }
  });

  db.on("error", function (err) {
    console.log("DB Error", err);
    if (err.code === "PROTOCOL_CONNECTION_LOST") {
      handleDisconnect();
    } else {
      throw err;
    }
  });

  console.log("Connected to DB");
};

export const getDb = () => {
  return db;
};
