import express from "express";
import "reflect-metadata";
import { handleDisconnect } from "./database";
import { updateWeather } from "./updateWeather";

const dotenv = require("dotenv");
dotenv.config();

const app: express.Application = express(),
  bodyParser = require("body-parser");
app.use(express.json());
app.use(bodyParser.json());

//Connect to and get DB instance
handleDisconnect();

let weather = require("./weather");
app.use("/api", weather);

//Update weather every 10 minutes
updateWeather();
setInterval(updateWeather, 1000 * 60 * 10);

app.listen(process.env.PORT || 8000, () =>
  console.log("Listening at Port " + process.env.PORT + "...")
);
