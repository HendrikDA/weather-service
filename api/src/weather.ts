import express, { Request, Response, NextFunction } from "express";
import "reflect-metadata";
import "body-parser";
const dotenv = require("dotenv");
dotenv.config();
import { getDb } from "./database";
import { updateWeather } from "./updateWeather";
import { resolve } from "dns";
let db = getDb();
const app: express.Application = express();

app.get("/weather/:location", function (req: any, res) {
  var location = req.params.location;
  let query = "SELECT * FROM location WHERE name ='" + location + "'";
  db.query(query, (err, rows) => {
    if (err) throw err;
    res.status(200).send(rows);
  });
});

app.get("/weather", async function (req: any, res) {
  //Get all locations
  let data = await getData();
  console.log(data);
});

function getData() {
  let query = "SELECT name FROM location";
  return new Promise((resolve) => {
    db.query(query, async (err, rows) => {
      if (err) throw err;
      for (const element of rows) {
        query =
          "SELECT * FROM location, weather WHERE location.id = weather.location_id && location.name = '" +
          element.name +
          "' LIMIT 1";
        const result = await iterateThroughElements(query);
        resolve(result);
      }
    });
  });
}

function iterateThroughElements(query) {
  let dashboardData = [];
  return new Promise((resolve) => {
    db.query(query, async (err, rows) => {
      if (err) throw err;
      const result = await iterateLocations(rows, dashboardData);
      resolve(result);
    });
  });
}

function iterateLocations(rows, dashboardData) {
  return new Promise(async (resolve) => {
    await rows.forEach((element) => {
      return new Promise((resolve) => {
        dashboardData.push(JSON.parse(JSON.stringify(element)));
        resolve(dashboardData);
      });
    });
    resolve(dashboardData);
  });
}

app.post("/weather/:location", function (req: any, res) {
  var newLocation = req.params.location;
  let query = "INSERT INTO location (name) VALUES('" + newLocation + "')";
  db.query(query, (err, rows) => {
    if (err) throw err;
    updateWeather();
    res.status(200).send(rows);
  });
});

app.get("/weather/:location/data", function (req: any, res) {
  let location = req.params.location;
  let query = "SELECT * FROM location WHERE name ='" + location + "'";

  query =
    "select * from location, weather where location.name = '" +
    location +
    "' && location.id = weather.location_id" +
    " ORDER BY weather.time ASC";
  db.query(query, (err, rows) => {
    if (err) throw err;
    res.status(200).send(rows);
  });
});

module.exports = app;
