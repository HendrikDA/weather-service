"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
require("reflect-metadata");
const database_1 = require("./database");
const updateWeather_1 = require("./updateWeather");
const dotenv = require("dotenv");
dotenv.config();
const app = express_1.default(), bodyParser = require("body-parser");
app.use(express_1.default.json());
app.use(bodyParser.json());
//Connect to and get DB instance
database_1.handleDisconnect();
let weather = require("./weather");
app.use("/api", weather);
//Update weather every 10 minutes
updateWeather_1.updateWeather();
setInterval(updateWeather_1.updateWeather, 1000 * 60 * 10);
app.listen(process.env.PORT || 8000, () => console.log("Listening at Port " + process.env.PORT + "..."));
//# sourceMappingURL=index.js.map