"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
require("reflect-metadata");
require("body-parser");
const dotenv = require("dotenv");
dotenv.config();
const database_1 = require("./database");
const updateWeather_1 = require("./updateWeather");
let db = database_1.getDb();
const app = express_1.default();
app.get("/weather/:location", function (req, res) {
    var location = req.params.location;
    let query = "SELECT * FROM location WHERE name ='" + location + "'";
    db.query(query, (err, rows) => {
        if (err)
            throw err;
        res.status(200).send(rows);
    });
});
app.get("/weather", function (req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        //Get all locations
        let data = yield getData();
        console.log(data);
    });
});
function getData() {
    let query = "SELECT name FROM location";
    return new Promise((resolve) => {
        db.query(query, (err, rows) => __awaiter(this, void 0, void 0, function* () {
            if (err)
                throw err;
            for (const element of rows) {
                query =
                    "SELECT * FROM location, weather WHERE location.id = weather.location_id && location.name = '" +
                        element.name +
                        "' LIMIT 1";
                const result = yield iterateThroughElements(query);
                resolve(result);
            }
        }));
    });
}
function iterateThroughElements(query) {
    let dashboardData = [];
    return new Promise((resolve) => {
        db.query(query, (err, rows) => __awaiter(this, void 0, void 0, function* () {
            if (err)
                throw err;
            const result = yield iterateLocations(rows, dashboardData);
            resolve(result);
        }));
    });
}
function iterateLocations(rows, dashboardData) {
    return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
        yield rows.forEach((element) => {
            return new Promise((resolve) => {
                dashboardData.push(JSON.parse(JSON.stringify(element)));
                resolve(dashboardData);
            });
        });
        resolve(dashboardData);
    }));
}
app.post("/weather/:location", function (req, res) {
    var newLocation = req.params.location;
    let query = "INSERT INTO location (name) VALUES('" + newLocation + "')";
    db.query(query, (err, rows) => {
        if (err)
            throw err;
        updateWeather_1.updateWeather();
        res.status(200).send(rows);
    });
});
app.get("/weather/:location/data", function (req, res) {
    let location = req.params.location;
    let query = "SELECT * FROM location WHERE name ='" + location + "'";
    query =
        "select * from location, weather where location.name = '" +
            location +
            "' && location.id = weather.location_id" +
            " ORDER BY weather.time ASC";
    db.query(query, (err, rows) => {
        if (err)
            throw err;
        res.status(200).send(rows);
    });
});
module.exports = app;
//# sourceMappingURL=weather.js.map