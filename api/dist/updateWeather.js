"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateWeather = void 0;
const dotenv = require("dotenv");
dotenv.config();
const request = require("request");
const database_1 = require("./database");
function getDbInstance() {
    let db = database_1.getDb();
    return db;
}
function emptyTable() {
    let db = getDbInstance();
    let query = "TRUNCATE TABLE weather";
    db.query(query, (err, rows) => {
        if (err)
            throw err;
    });
}
function updateWeather() {
    console.log("updating weather: " + new Date());
    let db = getDbInstance();
    emptyTable();
    let locations = [];
    let query = "SELECT * FROM location";
    db.query(query, (err, rows) => {
        if (err)
            throw err;
        rows.forEach((element) => {
            locations.push(element);
        });
        locations.forEach((element) => {
            let jsonData = {};
            let openWeather = "http://api.openweathermap.org/data/2.5/forecast?q=" +
                element.name +
                "&appid=" +
                process.env.APP_ID;
            request(openWeather, { json: true }, (err, res, body) => {
                if (err) {
                    return console.log(err);
                }
                for (let i = 0; i < 9; i++) {
                    jsonData["temp"] = res.body.list[i].main.temp;
                    jsonData["humidity"] = res.body.list[i].main.humidity;
                    jsonData["forecast"] = res.body.list[i].weather[0].main;
                    jsonData["forecast_description"] =
                        res.body.list[i].weather[0].description;
                    jsonData["icon_id"] = res.body.list[i].weather[0].icon;
                    jsonData["time"] = res.body.list[i].dt_txt;
                    jsonData["location_id"] = element.id;
                    let query = "INSERT INTO weather (temp, humidity, forecast, forecast_description, icon_id, location_id, time) " +
                        "VALUES(" +
                        jsonData["temp"] +
                        ", " +
                        jsonData["humidity"] +
                        ", '" +
                        jsonData["forecast"] +
                        "', '" +
                        jsonData["forecast_description"] +
                        "', '" +
                        jsonData["icon_id"] +
                        "', " +
                        jsonData["location_id"] +
                        ", '" +
                        jsonData["time"] +
                        "')";
                    db.query(query, (err, rows) => {
                        if (err)
                            throw err;
                    });
                }
            });
        });
    });
}
exports.updateWeather = updateWeather;
//# sourceMappingURL=updateWeather.js.map